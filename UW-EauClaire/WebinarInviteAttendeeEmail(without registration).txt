Hi there,

You are invited to a ${siteName} webinar.
<#if meetingTime??>
When: ${meetingTime}
    <#if isRecurring?? && isRecurring>
        <#if recurrenceRule??>
     ${recurrenceRule}
        </#if>
        <#if occurrences?? && ((occurrences?size) > 0)>
            <#list occurrences as occurrence>
     ${occurrence}
            </#list>
        </#if>
        <#if icalendarLinks?? && ((icalendarLinks?size) > 0)>
     Please download and import the following iCalendar (.ics) files to your calendar system.
            <#list icalendarLinks as icalendarLink>
     ${icalendarLink.key}: ${icalendarLink.value}
            </#list>
        </#if>
    </#if>
</#if>
<#if meetingTopic??>
Topic: ${(meetingTopic!'')?html}
</#if>

Please click the link below to join the webinar:
${joinUrl}
<#if password??>
Passcode: ${password}
</#if>
<#if !(isSimulive?? && isSimulive)>
<#if enablePSTN>
<#if isUserTSPEnabled?? && isUserTSPEnabled>
Or Telephone:
Dial: +1 312 626 6799 US (Chicago) 

Meeting ID: ${meetingNumber}
    </#if>
    <#if h323Password?? && enablePSTNPasswordProtected?? && !(isUserTSPEnabled??)>
Passcode: ${h323Password}
    </#if>
    <#if showInternationalNumbersLink?? && showInternationalNumbersLink>
Find your local number: ${teleConferenceUrl}
    </#if>
</#if>
</#if>
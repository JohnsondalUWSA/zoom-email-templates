Hi ${userName?html},  <br>
<br>
You are invited to a ${siteName} webinar.<br>
<br>
<#if meetingTime??>
Date Time: ${meetingTime} <br>
</#if>
<#if isRecurring?? && isRecurring>
<div class="cssTab">
    <#if recurrenceRule??>
    ${recurrenceRule}<br>
    </#if>
    <#if occurrences?? && ((occurrences?size) > 0)>
    <#list occurrences as occurrence>
    ${occurrence}<br>
    </#list>
    </#if>
    <#if icalendarLinks?? && ((icalendarLinks?size) > 0)>
    Please download and import the following iCalendar (.ics) files to your calendar system. <br>
    <#list icalendarLinks as icalendarLink>
    ${icalendarLink.key}: <a href="${icalendarLink.value}" target="_blank">${icalendarLink.value}</a><br>
    </#list>
    </#if>
</div>
<br>
</#if>
<#if topic??>
Topic: ${(topic!'')?html} <br>
</#if>
<br>
Join from a PC, Mac, iPad, iPhone or Android device: <br>
<div class="cssTab">
<#if isSendEmailFlag??>
    <a href="${joinUrl}">Click Here to Join</a><br>
 <#else>
     Please click this URL to join. <a href="${joinUrl}">${joinUrl}</a> <br>
</#if>
    Note: This link should not be shared with others; it is unique to you.<br>
	<#if password??>
    Passcode: ${password?html}<br>
	</#if>
<#if addToCalendarUrl?? && googleCalendarUrl?? && yahooCalendarUrl??>
    <a href="${addToCalendarUrl}">Add to Calendar</a> &nbsp; <#if !(account??) || !account.isGoogleCalendarDisabled()> <a href="${googleCalendarUrl}">Add to Google Calendar</a> &nbsp; </#if> <a href="${yahooCalendarUrl}">Add to Yahoo Calendar</a>
</#if>
</div>
<#if description??>
Description: ${description?html?replace("\n","<br/>\n")}
</#if>
<br/>
<#if enablePSTN>
<#if isUserTSPEnabled?? && isUserTSPEnabled>
<br>
Or Telephone: <br>
<div class="cssTab">
Dial: +1 312 626 6799 US (Chicago) 

Meeting ID: ${meetingNumber}
    </#if>
    <#if h323Password?? && enablePSTNPasswordProtected?? && !(isUserTSPEnabled??)>
Passcode: ${h323Password}
    </#if>
    <#if showInternationalNumbersLink?? && showInternationalNumbersLink>
Find your local number: ${teleConferenceUrl}
    </#if>
	<br>
    Webinar ID: ${meetingNumber} <br>
    <#if participantId??>
    Participant ID: ${participantId}<br>
	</#if>
	<#if h323Password?? && enablePSTNPasswordProtected??>
    Passcode: ${h323Password?html}<br>
	</#if>
    International numbers available: <a href="${teleConferenceUrl}">${teleConferenceUrl}</a>
</#if>
</div>
Hi there, 

You are invited to a ${siteName} webinar. 
<#if meetingTime??>
When: ${meetingTime} 
</#if>
<#if meetingTopic??>
Topic: ${(meetingTopic!'')?html}
</#if>

Register in advance for this webinar:
${registerUrl}

After registering, you will receive a confirmation email containing information about joining the webinar.

<#if speakers?? && (speakers?size > 0)>
----------

Webinar Speakers

<#list speakers as speaker>
${speaker.displayName!""} <#if (speaker.org?? && speaker.org != '') || (speaker.title?? && speaker.title != '') >(<#if speaker.title?? && speaker.title != ''>${(speaker.title!"")} </#if><#if speaker.org?? && speaker.org != ''>@${(speaker.org!"")}</#if>)</#if>
<#if (speaker.comment?? && speaker.comment != '')>
${(speaker.comment!"")?html}
</#if>

</#list>
</#if>
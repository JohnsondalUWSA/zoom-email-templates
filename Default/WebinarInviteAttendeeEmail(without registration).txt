Hi there,

You are invited to a ${siteName} webinar.
<#if meetingTime??>
When: ${meetingTime}
    <#if isRecurring?? && isRecurring>
        <#if recurrenceRule??>
     ${recurrenceRule}
        </#if>
        <#if occurrences?? && ((occurrences?size) > 0)>
            <#list occurrences as occurrence>
     ${occurrence}
            </#list>
        </#if>
        <#if icalendarLinks?? && ((icalendarLinks?size) > 0)>
     Please download and import the following iCalendar (.ics) files to your calendar system.
            <#list icalendarLinks as icalendarLink>
     ${icalendarLink.key}: ${icalendarLink.value}
            </#list>
        </#if>
    </#if>
</#if>
<#if meetingTopic??>
Topic: ${(meetingTopic!'')?html}
</#if>

Please click the link below to join the webinar:
${joinUrl}
<#if password??>
Passcode: ${password}
</#if>
<#if !(isSimulive?? && isSimulive)>
<#if enablePSTN>
<#if isUserTSPEnabled?? && isUserTSPEnabled>
Or Telephone:
    Dial:
    <#if tspNumbersInfo?? && ((tspNumbersInfo?size) > 0)>
        <#list tspNumbersInfo as tspNumberInfo>
            ${tspNumberInfo}
        </#list>
    </#if>
${(tspPsdTitle!'')?html}: ${(tspPsd!'')?html}
<#elseif pickedNumbers?? && ((pickedNumbers?size) > 0)>
    <#if supportOneTap?? && supportOneTap>
Or One tap mobile : <#assign phoneTapPwd = h323Password?? && enablePSTNPasswordProtected??>
    ${pickedNumbers[0].countryName!'US'}: ${pickedNumbers[0].displayNumber?replace(' ','')?replace('(0)','')},,${number?c}#<#if phoneTapPwd>,,,,*${h323Password}#</#if> <#if pickedNumbers[0].free>(Toll Free)</#if><#if ((pickedNumbers?size) > 1 && pickedNumbers[1].country == pickedNumbers[0].country)> or ${pickedNumbers[1].displayNumber?replace(' ','')?replace('(0)','')},,${number?c}#<#if phoneTapPwd>,,,,*${h323Password}#</#if> <#if pickedNumbers[1].free>(Toll Free)</#if></#if>
    </#if>
Or Telephone:
    Dial(for higher quality, dial a number based on your current location):<#assign n = 0><#list pickedNumbers as pickedNumber><#if (n == 0 || (n > 0 && pickedNumber.country != pickedNumbers[n - 1].country))>${'\r\n        '}${pickedNumber.countryName!'US'}: </#if><#if (n > 0 && pickedNumber.country == pickedNumbers[n - 1].country)> or </#if>${pickedNumber.displayNumber} <#if pickedNumber.free>(Toll Free)</#if><#assign n = n + 1></#list>
Webinar ID: ${meetingNumber}
    <#if h323Password?? && enablePSTNPasswordProtected??>
Passcode: ${h323Password}
	</#if>
    International numbers available: ${teleConferenceUrl}
</#if>
<#elseif useOtherAudioConference>

Or join by phone:

${otherAudioConferenceInfo!''}
</#if>
</#if>

<#if h323Gateway?? && ((h323Gateway?size) > 0)>
Or an H.323/SIP room system:
    H.323: <#if ((h323Gateway?size) <= 2)>${h323Gateway[0]}<#if ((h323Gateway?size) > 1)> or ${h323Gateway[1]}</#if></#if>
<#if ((h323Gateway?size) > 2)>
<#list h323Gateway as rc>
    ${rc}
</#list>
</#if>
    Meeting ID: ${meetingNumber}
    <#if h323Password??>
    Passcode: ${h323Password}
    </#if>
    <#if isCRC?? && isCRC>
    SIP: ${number?c}${(sipDomain!'@zoomcrc.com')?html}
    <#else>
    SIP: ${number?c}@${h323Gateway[0]}<#if ((h323Gateway?size) > 1)> or ${number?c}@${h323Gateway[1]}</#if>
    </#if>
    <#if h323Password??>
    Passcode: ${h323Password}
    </#if>
</#if>
Hi there, 

You are invited to a ${siteName} webinar. 
<#if meetingTime??>
When: ${meetingTime} 
</#if>
<#if meetingTopic??>
Topic: ${(meetingTopic!'')?html}
</#if>

Register in advance for this webinar:
${registerUrl}

<#if h323Gateway?? && ((h323Gateway?size) > 0)>
Or an H.323/SIP room system:
    H.323: <#if ((h323Gateway?size) <= 2)>${h323Gateway[0]}<#if ((h323Gateway?size) > 1)> or ${h323Gateway[1]}</#if></#if>
<#if ((h323Gateway?size) > 2)>
<#list h323Gateway as rc>
    ${rc}
</#list>
</#if>
    Meeting ID: ${meetingNumber}
    <#if h323Password??>
    Passcode: ${h323Password}
    </#if>
    <#if isCRC?? && isCRC>
    SIP: ${number?c}${(sipDomain!'@zoomcrc.com')?html}
    <#else>
    SIP: ${number?c}@${h323Gateway[0]}<#if ((h323Gateway?size) > 1)> or ${number?c}@${h323Gateway[1]}</#if>
    </#if>
    <#if h323Password??>
    Passcode: ${h323Password}
    </#if>
</#if>

After registering, you will receive a confirmation email containing information about joining the webinar.

<#if speakers?? && (speakers?size > 0)>
----------

Webinar Speakers

<#list speakers as speaker>
${speaker.displayName!""} <#if (speaker.org?? && speaker.org != '') || (speaker.title?? && speaker.title != '') >(<#if speaker.title?? && speaker.title != ''>${(speaker.title!"")} </#if><#if speaker.org?? && speaker.org != ''>@${(speaker.org!"")}</#if>)</#if>
<#if (speaker.comment?? && speaker.comment != '')>
${(speaker.comment!"")?html}
</#if>

</#list>
</#if>